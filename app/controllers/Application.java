package controllers;

import static play.data.Form.form;
import static play.libs.Json.toJson;

import java.util.List;

import models.Student;
import play.data.Form;
import play.db.ebean.Model;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.createAndList;
import views.html.update;

/**
 * This is controller of this Student CRUD Demo
 * 
 * @author Mahendra Bagul
 * @version 1.0
 * @since 2014-09-18
 */
public class Application extends Controller {

	/**
	 * This method is used to get the index Page which I have set to
	 * createAndList.scala.html
	 * 
	 * 
	 * @return Result This method returns createAndList.scala.html
	 */
	public static Result index() {
		Form<Student> studentForm = form(Student.class);
		return ok(createAndList.render(studentForm));
	}

	/**
	 * This method is used to save the student data to database
	 * 
	 * 
	 * @return Result This method redirects to createAndList.scala.html by
	 *         calling index() method
	 */
	public static Result saveStudent() {
		Student student = Form.form(Student.class).bindFromRequest().get();
		student.save();
		return redirect(routes.Application.index());
	}

	/**
	 * This method is used to get student data from database
	 * 
	 * 
	 * @return Result This method returns data in JSON format
	 */
	public static Result getStudents() {
		@SuppressWarnings("unchecked")
		List<Student> student = new Model.Finder(String.class, Student.class)
				.all();
		return ok(toJson(student));
	}

	/**
	 * This method is used to delete student specified by id.
	 * 
	 * @param id
	 *            This is the student id
	 * 
	 * @return Result This method redirects to createAndList.scala.html by
	 *         calling index() method
	 */
	public static Result deleteStudent(Long id) {
		Student.find.ref(id).delete();
		return redirect(routes.Application.index());
	}

	/**
	 * This method is used to get update form of student specified by id.
	 * 
	 * @param id
	 *            This is the student id
	 * 
	 * @return Result This method redirects to update.scala.html
	 */
	public static Result updateStudentGet(Long id) {
		Form<Student> studentForm = form(Student.class).fill(
				Student.find.byId(id));
		return ok(update.render(id, studentForm));
	}

	/**
	 * This method is used to save update form of student specified by id.
	 * 
	 * @param id
	 *            This is the student id
	 * 
	 * @return Result This method redirects to createAndList.scala.html
	 */
	public static Result updateStudentPost(Long id) {
		Form<Student> studentForm = form(Student.class).bindFromRequest();
		if (studentForm.hasErrors()) {
			return badRequest(update.render(id, studentForm));
		}
		studentForm.get().update(id);

		return redirect(routes.Application.index());
	}
}
