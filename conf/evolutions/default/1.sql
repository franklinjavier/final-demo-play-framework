# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table student (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  address                   varchar(255),
  gender                    varchar(255),
  birth_date                varchar(255),
  department                varchar(255),
  last_update               datetime not null,
  constraint pk_student primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table student;

SET FOREIGN_KEY_CHECKS=1;

